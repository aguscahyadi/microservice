USE [CustomerService]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 10/29/2023 20:39:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[id] [int] NOT NULL,
	[Name] [varchar](50) NULL,
	[Phone] [varchar](13) NULL,
	[Email] [varchar](50) NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Customers] ([id], [Name], [Phone], [Email]) VALUES (1, N'Agus Cahyadi', N'081522770415', N'aguscahyadi1988@gmail.com')
GO
