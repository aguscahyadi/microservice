﻿using CustomerService.Data;
using CustomerService.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CustomerService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private ApplicationDbContext _context;
        public CustomerController(ApplicationDbContext context)
        {
            _context = context;
        }
        // GET: api/<CustomerController>
        [HttpGet]
        public IEnumerable<Customer> Get()
        {
            return _context.Customers.ToList();
        }

        // GET api/<CustomerController>/5
        [HttpGet("{id}")]
        public Customer Get(int id)
        {
            return _context.Customers.Where(e => e.id == id).FirstOrDefault();
        }

        // POST api/<CustomerController>
        [HttpPost]
        public IActionResult Post([FromBody] Customer customer)
        {
            try
            {
                _context.Customers.Add(customer);
                _context.SaveChanges();
                //return StatusCode(StatusCodes.Status201Created, product);
                return StatusCode(StatusCodes.Status201Created, customer);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // PUT api/<CustomerController>/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Customer model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                var data = _context.Customers.Where(e => e.id == id).SingleOrDefault();
                if (data == null)
                {
                    return BadRequest();
                }
                else
                {

                    data.Name = model.Name;
                    data.Phone = model.Phone;
                    data.Email = model.Email;
                    _context.Customers.Update(data);
                    _context.SaveChanges();
                    return Ok(data);
                }
            }
        }

        // DELETE api/<CustomerController>/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if (id != 0)
            {
                var data = _context.Customers.Where(e => e.id == id).SingleOrDefault();
                if (data == null)
                {
                    return BadRequest();
                }
                else
                {
                    _context.Customers.Remove(data);
                    _context.SaveChanges();
                }

            }
            else
            {
                return BadRequest();
            }
            return Ok();
        }
    }
}
